package com.mimasrl.wireless_channels;

class GraphColoring {
    private int V, numOfColors;
    private int[] color;
    private int[][] graph;

    /**
     * Function to assign color
     **/
    int[] graphColor(int[][] g, int noc) {
        V = g.length;
        numOfColors = noc;
        color = new int[V];
        graph = g;

        try {
            solve(0);
            System.out.println("No solution");
        } catch (Exception e) {
            return color;
        }
        return null;
    }

    /**
     * function to assign colors recursively
     **/
    private void solve(int v) throws Exception {
        /* base case - solution found */
        if (v == V)
            throw new Exception("Solution found");
        /* try all colours */
        for (int c = 1; c <= numOfColors; c++) {
            if (isPossible(v, c)) {
                /* assign and proceed with next vertex */
                color[v] = c;
                solve(v + 1);
                /* wrong assignement */
                color[v] = 0;
            }
        }
    }

    /**
     * function to check if it is valid to allot that color to vertex
     **/
    private boolean isPossible(int v, int c) {
        for (int i = 0; i < V; i++)
            if (graph[v][i] == 1 && c == color[i])
                return false;
        return true;
    }
}