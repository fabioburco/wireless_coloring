package com.mimasrl.wireless_channels;

import org.apache.commons.cli.*;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.ext.*;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WirelessColoring {
    public static void main(String args[]) {
        Options options = new Options();

        Option inputOption = new Option("i", "input", true, "input file path");
        inputOption.setRequired(true);
        options.addOption(inputOption);

        Option channelOption = new Option("c", "channels", true, "channels file path");
        channelOption.setRequired(true);
        options.addOption(channelOption);

        Option bandOption = new Option("b", "band", true, "selected band (2g or 5g)");
        bandOption.setRequired(true);
        options.addOption(bandOption);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("wireless-channels", options);

            System.exit(1);
            return;
        }

        String inputFile = cmd.getOptionValue("input");
        BufferedReader br = openReader(inputFile);

        VertexProvider<CustomVertex> vertexProvider = (id, attributes) -> {
            CustomVertex cv = new CustomVertex(id);

            for (String a : attributes.keySet()) {
                String a1 = a.replaceAll("[,;/ ]", "");
                cv.attributes.put(a1, attributes.get(a));
            }

            return cv;
        };

        EdgeProvider<CustomVertex, DefaultEdge> edgeProvider =
                (from, to, label, attributes) -> new DefaultEdge();

        final String[] ID = new String[1];

        DOTImporter<CustomVertex, DefaultEdge> importer =
                new DOTImporter<>(vertexProvider, edgeProvider, null, (component, attributes) -> {
                    if (attributes.containsKey("ID"))
                        ID[0] = attributes.get("ID");
                });

        UndirectedGraph<CustomVertex, DefaultEdge> g =
                new SimpleGraph<>(DefaultEdge.class);

        try {
            assert br != null;
            importer.importGraph(g, br);
        } catch (ImportException e) {
            e.printStackTrace();
        }

        closeReader(br);

        String channelsFile = cmd.getOptionValue("channels");
        br = openReader(channelsFile);

        String band = cmd.getOptionValue("band");
        List<Integer> channelList = new ArrayList<>();
        String line;

        try {
            assert br != null;
            while ((line = br.readLine()) != null) {
                String[] split = line.split("=");
                // check to make sure you have valid data
                if (split[0].equals(band)) {
                    String[] values = split[1].split(",");
                    for (String value : values)
                        channelList.add(Integer.parseInt(value));
                }
            }
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }

        closeReader(br);

        int[][] adjencyMatrix = new int[g.vertexSet().size()][g.vertexSet().size()];

        List<CustomVertex> vertices = new ArrayList<>();

        vertices.addAll(g.vertexSet());

        for (DefaultEdge e : g.edgeSet()) {
            int i, j;
            CustomVertex source = g.getEdgeSource(e);
            CustomVertex target = g.getEdgeTarget(e);

            i = vertices.indexOf(source);
            j = vertices.indexOf(target);

            adjencyMatrix[i][j] = 1;
            adjencyMatrix[j][i] = 1;
        }

        GraphColoring gc = new GraphColoring();
        int[] coloredVertices = gc.graphColor(adjencyMatrix, channelList.size()); //use available channels

        for (CustomVertex v : vertices) {
            Color c = new Color(coloredVertices[vertices.indexOf(v)] - 1);
            v.setColor(c);
        }

        DOTExporter<CustomVertex, DefaultEdge> exporter = new DOTExporter<>(
                new StringComponentNameProvider<>(),
                null,
                null,
                component -> {
                    Map<String, String> map = component.attributes;
                    map.put("suggested_channel", channelList.get(component.getColor().value).toString());
                    return map;
                },
                null,
                component -> ID[0]
        );

        try {
            exporter.exportGraph(g, new FileOutputStream(new File(inputFile.replace(".dot", "_optimized.dot"))));
        } catch (ExportException | FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static BufferedReader openReader(String file) {
        FileReader fr;
        BufferedReader br;

        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            return br;
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        return null;
    }

    private static void closeReader(BufferedReader br) {
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class Color {
        private int value;

        Color(int value) {
            this.value = value;
        }

        public String toString() {
            return "" + value;
        }
    }

    /**
     * A custom graph vertex.
     */
    static class CustomVertex {
        private String id;
        private Color color;
        private Map<String, String> attributes = new HashMap<>();

        CustomVertex(String id) {
            this(id, null);
        }

        CustomVertex(String id, Color color) {
            this.id = id;
            this.color = color;
        }

        @Override
        public int hashCode() {
            return (id == null) ? 0 : id.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            CustomVertex other = (CustomVertex) obj;
            if (id == null) {
                return other.id == null;
            } else {
                return id.equals(other.id);
            }
        }

        Color getColor() {
            return color;
        }

        void setColor(Color color) {
            this.color = color;
        }

        @Override
        public String toString() {
            return this.id;
        }
    }
}
